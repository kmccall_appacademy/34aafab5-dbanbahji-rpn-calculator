class RPNCalculator
  attr_accessor :stack

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def pop
    @stack.delete_at(-1)
  end

  def plus
    perform_operation(:+)
  end

  def minus
    perform_operation(:-)
  end

  def times
    perform_operation(:*)
  end

  def divide
    perform_operation(:/)
  end

  def value
    @stack.last
  end

  def tokens(string)
    tokens = string.split
    tokens.map {|char| operations(char)? char.to_sym : Integer(char)}
  end

  def evaluate(string)
    token = tokens(string)
    token.each do |token|
      case token
      when Integer
        push(token)
      else
        perform_operation(token)
      end
      end
    self.value
  end


  def operations(char)
    [:+, :-, :*, :/].include?(char.to_sym)
  end


  def perform_operation(op_symbol)
    raise 'calculator is empty' if @stack.length < 2
    @second_num = @stack.pop.to_f
    @first_num = @stack.pop.to_f

    case op_symbol
    when :+
      @stack << @first_num + @second_num
    when :-
      @stack << @first_num - @second_num
    when :*
      @stack << @first_num * @second_num
    when :/
      @stack << @first_num / @second_num
    else
      @stack.push(@first_num)
      @stack.push(@second_num)
      raise 'invalid operation'
    end
  end
end
